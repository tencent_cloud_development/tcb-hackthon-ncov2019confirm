## 同乘码

## 背景

疫情爆发时期, 同乘交通工具会有交叉感染的风险, 我们经常在各主流新闻媒体看到类似**「急寻某月某日\*车次的乘客」**, 另一方面也有不少工具可以提供查询某次交通工具是否发生过疫情的功能, 但是不可能发生疫情的交通班次上的所有乘客都会关注这些信息或主动反复去查询, 这就导致信息不能有效传递给需要的人。

## 方案

趁着前几天腾讯云云开发组织的一次公益黑客马拉松活动的机会, 我和另一位同事开发一个**「同乘码」**小程序, 乘客可以在乘坐交通时扫码订阅(登记)对应交通班次的信息, 后期如果有同乘旅客上报或官方公布疫情信息, 小程序会通过微信的消息订阅功能主动向扫过码的乘客发送消息, 实现疫情信息的高效传递。目前这个**小程序已开源**, 使用者可以任何方式使用。

## 主要功能
![Image text](img/summary.png)

- 首页可以查询和订阅, 订阅后还可以分享二维码给同乘的旅客

- 我的同乘码, 显示已乘坐的交通工具

- 进入到同乘界面, 有上报和查看是否有疫情的功能, 还可以到成员列表页面, 可以实时看到同乘的旅客, 另外实时留言功能可能实时看到其他人的留言



## 特性

1. 基于WeUI开发的UI，界面简洁友好
2. 基于缓存+存储的查询，高性能
3. 基于小程序云开发，无服务化，部署更简单
4. 基于微信疫情同行订阅消息，接收更及时
5. 基于用户上去上报+外部数据源实时推送，来源更全更灵活



## 部署方式

### 1)下载

下载源码

```shell
git clone https://gitee.com/tencent_cloud_development/tcb-hackthon-ncov2019confirm.git
```

### 2)配置

修改appid

project.config.json里修改appid

```json
{
    // ...
    "appid": "自己的appid"
    // ...
}
```

微信开发者工具导入项目，开通“云开发”，新建云开发环境，得到evn_id，并配置`config`/`config.js`

```json
{
    "env": "xxx",		// 新建的云开发环境的id，注意是id不是name
    "traceUser": true	// 是否追踪用户 true / false
}
```



云开发控制台 - 数据库 - 新建集合

- admin：管理员信息表，审核自主上报病例用
- chatroom: 聊天/留言信息, 需要给groupId加非唯一索引
- member：同乘用户列表, 需要分别给roomId和openId加非唯一索引
- report：疫情上报信息, 需要给roomId加非唯一索引
- roomInfo：交通工具信息, 需要给key加唯一索引.
- traffic：外部确认病例交通班次表

集合结构如下：

**admin**集合

| field  | type   |
| ------ | ------ |
| openId | string |

**member**集合

| field    | type   |
| -------- | ------ |
| opendId  | string |
| roomId   | string |
| src      | string |
| status   | number |
| userInfo | object |

**report**集合

| field  | type   |
| ------ | ------ |
| date   | string |
| name   | string |
| phone  | string |
| review | number |
| roomId | string |
| rtype  | string |

**roomInfo**集合

| field     | type   |
| --------- | ------ |
| date      | string |
| key       | string |
| typeName  | string |
| typeValue | string |
| wxacode   | string |

**traffic**集合

| field       | type   |
| ----------- | ------ |
| created_at  | string |
| source      | string |
| t_date      | string |
| t_end       | string |
| t_memo      | string |
| t_no        | string |
| t_no_sub    | string |
| t_pos_end   | string |
| t_pos_start | string |
| t_start     | string |
| t_type      | string |
| updated_at  | string |
| verified    | number |
| who         | string |

新建后，修改权限为“**所有用户可读，仅创建者可读写**”！！！

### 3)部署云函数

login`云函数目录，右键 - 上传并部署

`openapi`云函数目录，右键 - 上传并部署

`sendmsg`云函数目录，右键 - 上传并部署

### 4)订阅消息模板配置

- 订阅消息模板的申请方法：在 [https://mp.weixin.qq.com/](https://mp.weixin.qq.com/)  上申请自定义的推送模板，“功能” - “订阅消息” - “添加”, 注意参数的格式要一致；

- 订阅消息模板的设置方法：cloudfunctions - sendmsg - index.js - templateId 参数配置为自定义的订阅消息模板ID即可；

### 5)完成部署

Ctrl + S，即可看到项目Running起来啦！



## HTTP接口说明

小程序可以通过利用云开发提供的HTTP接口进行管理, HTTP接口也是基于云开发的云函数开发, 需要在腾讯云官网的云开发管理页面添加对应的函数, 并开启HTTP访问. 目前接口还没有权限校验功能, 可以在代码里加上一个skey之类来实现.

接口列表:

- sendmsg 给某个乘客发送一条消息, 注意要先在微信管理后台申请好消息模板.
- confirmReport 对某一条用户上报的疫情信息进行确认.
- getReport 获取所有未审核的信息上报列表
- addReport 上报一条官方发布的疫情信息

接口参数可以看源码



## 源码结构

源码主要分成两部分

1. miniprogram目录, 小程序的源码, 与官方默认的代码结构一致.
2. cloudfuntion目录, 云函数代码, 使用nodejs实现.



## 技术架构

客户端使用原生小程序开发, 服务端全部使用云开发
![Image text](https://ask.qcloudimg.com/draft/1333088/kn2p6njw6c.png?imageView2/2/w/1620)



## 小程序码

![poster](img/poster.jpg)

## Contributors

- defool

- kapt4inw01f

