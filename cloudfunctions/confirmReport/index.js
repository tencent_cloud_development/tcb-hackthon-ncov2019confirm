// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()

/*
 审核通过用户上报信息

 @param skey 云函数httpLogin中定义的密钥
 @param reportId 上报信息对应的_id值
 @param remark 说明备注

*/
exports.main = async (event, context) => {
  try {
    var req = JSON.parse(event.body)
    var verify = await cloud.callFunction({
      name: 'httpLogin',
      data: {
        skey: req.skey
      }
    })

    if(!verify.result || !verify.result.matched){
      return {"error": "wrong skey"}
    }
    
    return await db.collection("report").doc(req.reportId).update({
      review: "已审核",
      remark: req.remark,
    })
  } catch(e) {
    console.error(e)
  }
}