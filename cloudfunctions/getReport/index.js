// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()

/*
  获取未审核的列表

  @param skey 云函数httpLogin中定义的密钥
*/
exports.main = async (event, context) => {
  try {
    var req = JSON.parse(event.body)
    var verify = await cloud.callFunction({
      name: 'httpLogin',
      data: {
        skey: req.skey
      }
    })

    if(!verify.result || !verify.result.matched){
      return {"error": "wrong skey"}
    }

    return await db.collection('report').where({
      review: db.command.exists(false),
    }).get()
  } catch(e) {
    console.error(e)
  }
}