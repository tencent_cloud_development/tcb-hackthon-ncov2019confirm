// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db = cloud.database()

/*
  上报一条官方发布的疫情信息

  @param skey 云函数httpLogin中定义的密钥
  @param trafficId 交通信息ID
*/
exports.main = async (event, context) => {
  try {
    var req = JSON.parse(event.body)
    var verify = await cloud.callFunction({
      name: 'httpLogin',
      data: {
        skey: req.skey
      }
    })

    if(!verify.result || !verify.result.matched){
      return {"error": "wrong skey"}
    }

    return await db.collection('report').add({
      roomId: req.trafficId,
      name: '***',
      phone: '****',
      rtype: '已确诊',
    })
  } catch(e) {
    console.error(e)
  }
}