let getDateStr = function() {
  var dd = new Date()
  return dd.getFullYear().toString() + '-' + (dd.getMonth() + 1).toString() + '-' + dd.getDate().toString()
}

let translateDate = function(ds) {
  const date = ds.split('-')
  let items = []
  items.push(date[0])
  date[1][0] === '0' ? items.push(date[1].slice(1)) : items.push(date[1])
  date[2][0] === '0' ? items.push(date[2].slice(1)) : items.push(date[2])
  return items.join('/')
}

module.exports = {
  getDateStr,
  translateDate
}
