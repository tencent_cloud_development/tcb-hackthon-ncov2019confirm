// miniprogram/pages/report/report.js
const util = require('../../util/tools.js')
const db = wx.cloud.database()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    roomId: 0,
    radioItems: [{
        name: '疑似',
        value: 'uncertain',
        checked: true
      },
      {
        name: '确诊',
        value: 'confirm'
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.data.roomId = options.roomId
  },

  // 提交表单上报疫情
  submitForm: function(e) {
    let vals = e.detail.value
    if (!vals.rtype) {
      wx.showToast({
        title: '请选择类型',
        icon: 'none',
      })
      return
    }
    if (!vals.name) {
      wx.showToast({
        title: '请输入姓名',
        icon: 'none',
      })
      return
    }
    if (!vals.phone) {
      wx.showToast({
        title: '请输入联系方式',
        icon: 'none',
      })
      return
    }
    if (!(/^1[3456789]\d{9}$/.test(vals.phone))) {
      wx.showToast({
        title: '手机号码格式错误',
        icon: 'none',
      })
      return
    }
    wx.showToast({
      title: '正在提交',
      icon: 'none',
    })
    vals.roomId = this.data.roomId
    vals.date = util.getDateStr()
    vals.review = 0

    db.collection("report").add({
      data: vals,
      success: res => {
        wx.showToast({
          title: '提交成功, 返回上页...',
          duration: 3000,
        })
        setTimeout(wx.navigateBack, 3000)
      },
      fail: console.log,
    })
  },

  // 病例类型变化处理
  onTypeChange: function(e) {
    var radioItems = this.data.radioItems;
    for (var i = 0, len = radioItems.length; i < len; ++i) {
      radioItems[i].checked = radioItems[i].value == e.detail.value;
    }
    this.setData({
      radioItems: radioItems
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})