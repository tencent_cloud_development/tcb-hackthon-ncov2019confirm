// miniprogram/pages/member/member.js

const app = getApp()

const db = wx.cloud.database()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    memberNum: 0,
    members: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var roomId = options.roomId
    var self = this
    wx.showLoading({
      title: "加载中..."
    })
    db.collection("member").where({roomId: roomId, status: 0}).watch({
      onChange: function(e){
        // todo: add delay
        console.log("docs", e.docs)
        self.setData({
          memberNum: e.docs.length,
          members: e.docs,
        })
        wx.hideLoading()
      },
      onError: function(e){
        wx.hideLoading()
        wx.showToast({
          title: '内部错误, 请重新打开小程序',
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})