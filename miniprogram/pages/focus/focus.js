// miniprogram/pages/focus/focus.js
const app = getApp()
const db = wx.cloud.database()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: './user-unlogin.png',
    userInfo: {},
    logged: false,
    takeSession: false,
    requestResult: '',
    roomId: '',
    showMine: '',
    mylist: [],
    showText: '显示我的关注',
    typeMap: {
      airplane: '飞机',
      train: '火车',
      car: '汽车',
      ship: '轮船',
      other: '其他',
    },
    isAdmin: false
  },

  // 获取我的订阅
  showMine: function() {
    var self = this
    console.log("show openid", app.globalData.openid)
    db.collection("member").where({
      openId: app.globalData.openid,
      status: 0
    }).get({
      success: res => {
        console.log("res", res)
        let roomIds = []
        for (let i in res.data) {
          if (res.data[i].roomId) {
            roomIds.push(res.data[i].roomId)
          }
        }
        if (roomIds.length == 0) {
          wx.showToast({
            title: '无内容',
          })
          self.setData({
            mylist: [],
          })
          return
        }

        db.collection('roomInfo').where({
          _id: db.command.in(roomIds)
        }).get({
          success: res => {
            self.setData({
              showText: '收起我的关注',
              showMine: 'Y',
              mylist: res.data,
            })
          }
        })
        fail: console.log
      },
      fail: console.log
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.checkAdmin()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.showMine()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 判断是否为管理员
  checkAdmin() {
    db.collection('admin').where({
      openId: app.globalData.openid
    }).get().then(res => {
      if (res.errMsg === 'collection.get:ok' && res.data.length > 0) {
        this.setData({
          isAdmin: true
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }
})