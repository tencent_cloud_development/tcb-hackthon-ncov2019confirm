// miniprogram/pages/audit/audit.js
const db = wx.cloud.database()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    reports: [],
    slideButtons: [{
      text: '通过'
    }, {
      type: 'warn',
      text: '驳回',
    }]
  },

  audit: function(_id, action) {
    db.collection('report').doc(_id).update({
      data: {
        review: action
      },
      success: res => {
        wx.showToast({
          title: '审核成功',
        })
        this.getReports()
      },
      fail: err => {
        console.log(err)
        wx.showToast({
          title: '审核失败',
        })
      }
    })
  },

  onAuditTap: function(e) {
    const _id = e.currentTarget.dataset.id
    if (e.detail.index === 0) {
      // 通过
      this.audit(_id, 1)
    } else {
      // 驳回
      this.audit(_id, 2)
    }
  },

  getReports() {
    db.collection('report').where({
      review: 0
    }).get({
      success: res => {
        this.setData({
          reports: res.data
        })
      },
      fail: console.log
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getReports()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})